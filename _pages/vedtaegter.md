---
layout: page
title: Vedtægter
---

<meta http-equiv="Refresh" content="https://git.data.coop/data.coop/dokumenter/src/branch/master/Vedtaegter.md" />

# Du bliver nu viderestillet til den seneste version af vores vedtægter

Hvis der ikke sker noget kan du trykke på følgende link:

<a href="https://git.data.coop/data.coop/dokumenter/src/branch/master/Vedtaegter.md">https://git.data.coop/data.coop/dokumenter/src/branch/master/Vedtaegter.md</a>

