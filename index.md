---
layout: page
---
<div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: #000" id="why">
<div style="margin: auto; padding: 50px; font-size: 28px; color: #ddd; text-align: center;">

<p><strong>🌈 I mindet om Graffen 💖</strong></p>

<p>Kæreste Jesper, data.coop fortsætter i dit minde og i din ære. Du har forladt os alt for tidligt, men du har givet os mere end nogen anden. Vi kunne blot ønske os at du var blevet noget længere. Du har været vores helt, nu er du vores legende.</p>

<p>Tak for alt.</p>

<p>
  <a href="#" onclick="document.getElementById('why').style.display='none';" style="color: #fff; text-decoration: underline; font-size: 18px;">til vores forside »</a>
</p>

</div>

</div>
# Velkommen til data.coop

Vi er en forening som har formålet, at passe på medlemmernes data. Vores
kerneprincipper er:

- Privatlivsbeskyttelse
- Kryptering
- Decentralisering
- Zero-knowledge

Ud fra de kerneprincipper udbyder vi onlinetjenester til medlemmerne.
Hovedtanken er, at vi som udgangspunkt stoler mere på hinanden end på "de
store" som f.eks. Google, Microsoft eller Facebook.

Foreningen holdt stiftende generalforsamling i 2014 og blev genstartet i 2016.
Vi arbejder løbende på, at udbygge aktiviteterne.

[Læse mere om hvad du får ud af et medlemsskab](/tjenester/) og
[hvordan du kan melde dig ind](/medlem/) og være med til at opbygge noget fedt.
